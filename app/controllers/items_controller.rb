class ItemsController < ApplicationController
  def index
    @items = Item.all
  end

  def add_to_cart
    current_cart.add_item(item)
    redirect_to cart_path
  end

  def remove_from_cart
    current_cart.remove_item(item)
    redirect_to cart_path
  end

  private

  def item
    @item ||= Item.find(params[:id])
  end
end
