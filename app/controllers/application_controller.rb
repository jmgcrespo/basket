class ApplicationController < ActionController::Base
  after_action :set_current_cart
  helper_method :current_cart
  protect_from_forgery with: :exception

  private

  def current_cart
    Cart.find_or_create_by(id: session[:cart])
  end

  def set_current_cart
    session[:cart] = current_cart.id
  end
end
