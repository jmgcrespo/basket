class PromotionCodesController < ApplicationController
  def create
    @promotion_code = PromotionCode.find_by(name: params[:name])

    current_cart.carts_promotion_codes.find_or_create_by(promotion_code: @promotion_code).save

    redirect_to cart_path
  end

  def destroy
    current_cart.promotion_codes.destroy(promotion_code)
    redirect_to cart_path
  end

  private

  def promotion_code
    @promotion_code ||= PromotionCode.find(params[:id])
  end
end
