class OrdersController < ApplicationController
  def new
    @cart = current_cart
  end

  def create
    @order = Order.from_cart(current_cart)
    @order.assign_attributes(order_params)

    if @order.save
      current_cart.clear
      redirect_to @order
    else
      render :new
    end
  end

  def show
    @order = Order.find(params[:id])
  end

  private

  def order_params
    params.require(:order).permit(:email, :address, :credit_card, :cart_version)
  end
end
