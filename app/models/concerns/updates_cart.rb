module UpdatesCart
  extend ActiveSupport::Concern
  included do
    after_commit :update_cart_total
  end

  def update_cart_total
    cart.update_total
  end
end
