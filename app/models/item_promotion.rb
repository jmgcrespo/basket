class ItemPromotion < ApplicationRecord
  belongs_to :item
  validates :quantity, :price, :item, presence: true

  def perform count
    promotion_price count
  end

  def promotion_price count
    (promoted, remain) = count.divmod(quantity)
    price * promoted + item.price * remain
  end
end
