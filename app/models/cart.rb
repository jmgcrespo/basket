class Cart < ApplicationRecord
  has_many :carts_items, inverse_of: :cart
  has_many :items, through: :carts_items
  has_many :carts_promotion_codes, inverse_of: :cart
  has_many :promotion_codes, through: :carts_promotion_codes

  def add_item item
    cart_item = self.carts_items.find_or_initialize_by(item: item)
    cart_item.increase_count
    cart_item.save
  end

  def remove_item item
    if cart_item = carts_items.find_by(item: item)
    cart_item.decrease_count
    cart_item.save
    end
  end

  def total
    total_with_promotion_codes
  end

  def items_total
    @items_total_price ||= get_items_total
  end

  def get_items_total
    carts_items.inject(0) do |items_price, item|
      items_price += item.price
    end
  end

  def total_with_promotion_codes
    promotion_codes.inject(items_total) do |discounted_total, code|
      code.perform(discounted_total)
    end.round(2)
  end

  def clear
    carts_items.delete_all
    carts_promotion_codes.delete_all
  end
end
