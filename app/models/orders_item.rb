class OrdersItem < ApplicationRecord
  belongs_to :order, inverse_of: :orders_items
  belongs_to :item

  delegate :name, to: :item
end
