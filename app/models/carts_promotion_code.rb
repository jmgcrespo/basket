class CartsPromotionCode < ApplicationRecord

  belongs_to :cart, inverse_of: :carts_promotion_codes
  belongs_to :promotion_code

  validates_uniqueness_of :promotion_code_id, scope: :cart_id
  validates_associated :cart
  validates_uniqueness_of :cart_id, if: :solo_promotion_code?
  validate :no_other_solo

  def solo_promotion_code?
    promotion_code.solo?
  end

  def no_other_solo
    errors.add(:promotion_code, 'already a solo promotion') if cart.promotion_codes.where(solo: true).present?
  end
end
