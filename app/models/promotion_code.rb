class PromotionCode < ApplicationRecord
  CODE_TYPES = {
    'percentage_off' => CodeTypes::PercentageOff,
    'amount_off' => CodeTypes::AmountOff,
  }

  has_many :carts_promotion_codes
  has_many :carts, through: :carts_promotion_codes
  has_many :orders_promotion_codes
  has_many :orders, through: :orders_promotion_codes

  validates :name, :parameter, presence: true
  validate :valid_code_type?

  def perform value
    CODE_TYPES[code_type].new(parameter).perform value
  end

  private
  def valid_code_type?
    CODE_TYPES.include?(code_type)
  end
end
