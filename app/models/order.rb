class Order < ApplicationRecord
  has_many :orders_items, inverse_of: :order
  has_many :items, through: :orders_items
  has_many :orders_promotion_codes, inverse_of: :order
  has_many :promotion_codes, through: :orders_promotion_codes
  belongs_to :cart

  validates :email, :address, :credit_card, presence: true
  validate :check_cart_version

  attr_accessor :cart_version

  def self.from_cart cart
    new.tap do |order|
      order.cart = cart
      order.total = cart.total

      cart.carts_items.each do |item|
        order.orders_items.build(count: item.count, price: item.price, item_id: item.item_id)
      end

      cart.promotion_codes.each do |promotion_code|
        order.orders_promotion_codes.build(promotion_code_id: promotion_code.id)
      end
    end
  end

  def check_cart_version
    cart_version == cart
  end
end
