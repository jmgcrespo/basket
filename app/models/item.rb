class Item < ApplicationRecord
  has_many :carts_items
  has_many :carts, through: :carts_items
  has_one :item_promotion
  has_many :orders_items, inverse_of: :item
  has_many :orders, through: :orders_items
  validates :name, :price, presence: true
end
