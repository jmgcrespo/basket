class OrdersPromotionCode < ApplicationRecord

  belongs_to :order, inverse_of: :orders_promotion_codes
  belongs_to :promotion_code
end
