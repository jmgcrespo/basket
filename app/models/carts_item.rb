class CartsItem < ApplicationRecord

  belongs_to :cart, inverse_of: :carts_items
  belongs_to :item
  before_save :destroy_if_count_0

  delegate :name, to: :item

  def increase_count
    self.count += 1
  end

  def decrease_count
    self.count -= 1
  end

  def price
    calculate_item_price.round(2)
  end

  private

  def calculate_item_price
    item.item_promotion.present? ? item.item_promotion.perform(self.count) : item.price * self.count
  end

  def destroy_if_count_0
    destroy if count <= 0
  end

end
