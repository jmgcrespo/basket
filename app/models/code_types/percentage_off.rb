module CodeTypes
  class PercentageOff < Base
    def promote_price price
      price * (1 - parameter)
    end
  end
end
