module CodeTypes
  class Base
    attr_reader :parameter
    def initialize parameter
      @parameter = parameter
    end
    
    def perform old_price
      new_price = promote_price old_price
      new_price <= 0 ? 0 : new_price
    end
  end
end
