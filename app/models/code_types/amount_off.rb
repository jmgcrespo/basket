module CodeTypes
  class AmountOff < Base
    def promote_price price
      price - parameter
    end
  end
end
