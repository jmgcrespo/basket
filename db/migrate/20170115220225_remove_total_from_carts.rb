class RemoveTotalFromCarts < ActiveRecord::Migration[5.0]
  def change
    remove_column :carts, :total
  end
end
