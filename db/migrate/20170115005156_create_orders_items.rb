class CreateOrdersItems < ActiveRecord::Migration[5.0]
  def change
    create_table :orders_items do |t|
      t.references :order, foreign_key: true
      t.references :item, foreign_key: true
      t.float :price
      t.integer :count
    end
  end
end
