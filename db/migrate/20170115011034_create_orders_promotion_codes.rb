class CreateOrdersPromotionCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :orders_promotion_codes do |t|
      t.references :order, foreign_key: true
      t.references :promotion_code, foreign_key: true
    end
  end
end
