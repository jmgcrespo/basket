class RemovePriceFromCartsItems < ActiveRecord::Migration[5.0]
  def change
    remove_column :carts_items, :price
  end
end
