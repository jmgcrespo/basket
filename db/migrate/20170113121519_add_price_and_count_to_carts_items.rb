class AddPriceAndCountToCartsItems < ActiveRecord::Migration[5.0]
  def change
    add_column :carts_items, :price, :float
    add_column :carts_items, :count, :integer, default: 0
  end
end
