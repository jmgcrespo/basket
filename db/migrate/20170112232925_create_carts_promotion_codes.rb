class CreateCartsPromotionCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :carts_promotion_codes do |t|
      t.references :promotion_code, foreign_key: true
      t.references :cart, foreign_key: true
    end
  end
end
