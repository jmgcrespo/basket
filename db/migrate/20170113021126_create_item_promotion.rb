class CreateItemPromotion < ActiveRecord::Migration[5.0]
  def change
    create_table :item_promotions do |t|
      t.integer :quantity
      t.references :item, foreign_key: true
      t.float :price
    end
  end
end
