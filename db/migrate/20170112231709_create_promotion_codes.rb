class CreatePromotionCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :promotion_codes do |t|
      t.string :name
      t.float :parameter
      t.string :code_type
      t.boolean :solo, default: false
    end
  end
end
