class CreateOrder < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :email
      t.string :address
      t.string :credit_card
      t.references :cart, foreign_key: true
      t.float :price
    end
  end
end
