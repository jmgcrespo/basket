class CreateCartsItems < ActiveRecord::Migration[5.0]
  def change
    create_table :carts_items do |t|
      t.references :cart, foreign_key: true
      t.references :item, foreign_key: true
    end
  end
end
