# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170115220601) do

  create_table "carts", force: :cascade do |t|
    t.datetime "updated_at"
    t.datetime "created_at"
  end

  create_table "carts_items", force: :cascade do |t|
    t.integer "cart_id"
    t.integer "item_id"
    t.integer "count",   default: 0
    t.index ["cart_id"], name: "index_carts_items_on_cart_id"
    t.index ["item_id"], name: "index_carts_items_on_item_id"
  end

  create_table "carts_promotion_codes", force: :cascade do |t|
    t.integer "promotion_code_id"
    t.integer "cart_id"
    t.index ["cart_id"], name: "index_carts_promotion_codes_on_cart_id"
    t.index ["promotion_code_id"], name: "index_carts_promotion_codes_on_promotion_code_id"
  end

  create_table "item_promotions", force: :cascade do |t|
    t.integer "quantity"
    t.integer "item_id"
    t.float   "price"
    t.index ["item_id"], name: "index_item_promotions_on_item_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "name"
    t.float  "price"
  end

  create_table "orders", force: :cascade do |t|
    t.string  "email"
    t.string  "address"
    t.string  "credit_card"
    t.integer "cart_id"
    t.float   "total"
    t.index ["cart_id"], name: "index_orders_on_cart_id"
  end

  create_table "orders_items", force: :cascade do |t|
    t.integer "order_id"
    t.integer "item_id"
    t.float   "price"
    t.integer "count"
    t.index ["item_id"], name: "index_orders_items_on_item_id"
    t.index ["order_id"], name: "index_orders_items_on_order_id"
  end

  create_table "orders_promotion_codes", force: :cascade do |t|
    t.integer "order_id"
    t.integer "promotion_code_id"
    t.index ["order_id"], name: "index_orders_promotion_codes_on_order_id"
    t.index ["promotion_code_id"], name: "index_orders_promotion_codes_on_promotion_code_id"
  end

  create_table "promotion_codes", force: :cascade do |t|
    t.string  "name"
    t.float   "parameter"
    t.string  "code_type"
    t.boolean "solo",      default: false
  end

end
