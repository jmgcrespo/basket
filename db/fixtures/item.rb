Item.seed(:id,
{ id: 1, name: 'Smart Hub', price: 49.99 },
{ id: 2, name: 'Motion Sensor', price: 24.99 },
{ id: 3, name: 'Wireless Camera', price: 99.99 },
{ id: 4, name: 'Smoke Sensor', price: 19.99 },
{ id: 5, name: 'Water Leak Sensor', price: 14.99 }
)
