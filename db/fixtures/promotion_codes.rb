PromotionCode.seed(
  :id,
  { id: 1, name: '20%OFF', code_type: 'percentage_off', parameter: 0.2, solo: true },
  { id: 2, name: '5%OFF', code_type: 'percentage_off', parameter: 0.05 },
  { id: 3, name: '20POUNDSOFF', code_type: 'amount_off', parameter: 20 }
)
