# BASKET #
A great app with a great name.

## Summary ##

[DB Models](https://bitbucket.org/jmgcrespo/basket/raw/5616e9b0ec9db6888af2ecbd535b9e410fbc2566/erd.pdf)

A Cart instance is associated to the user session. Which allows the app to keep added products if orders is not submitted.

Items and Promotion Codes are added to a Cart model. The amount of items added to cart and its cost, including Quantity Promotions are managed bye the association model Carts-Items, which then is used by the Cart that is able to calculate final price after processing promotion codes.

Promotion Codes are processed in the same order user adds them which is not ideal as order affects final discount. Promotion Codes discount is applied after Quantity Discounts

An Order model has same association as a cart. On order creation calculated price for items and order total is stored in the order.

Its is assumed that Items and Promotion Codes will be soft deleted.

## Installation ##

* Ruby version

      2.3.1

      gem install bundle
      bundle install

* Initialise DB

      rake db:create
      rake db:migrate
      rake db:seed_fu

* Start the App

      rails s

      Point your browser to http://localhost:3000/


* How to run the test suite

      rspec spec/