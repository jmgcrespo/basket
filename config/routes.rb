Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'items#index'

  resources :items, only: [:index] do
    member do
     post  :add_to_cart
     post  :remove_from_cart
    end
  end

  resource :cart, only: [:show] do
    resources :promotion_codes, only: [:index, :create, :destroy]
  end

  resources :orders, only: [:new, :create, :show] do
  end
end
