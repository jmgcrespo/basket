require 'rails_helper'

RSpec.describe 'Promotions', type: :feature do
  let!(:item1) { FactoryGirl.create(:item, name: 'Item1', price: 44.11) }
  let!(:item2) { FactoryGirl.create(:item, name: 'Item2', price: 34.11) }
  let!(:item3) { FactoryGirl.create(:item, name: 'Item3', price: 24.11) }
  let!(:code_20per) { FactoryGirl.create(:promotion_code) }
  let!(:code_20off) { FactoryGirl.create(:amount_off) }
  let!(:code_5off) { FactoryGirl.create(:amount_off, name: '5POUNDSOFF', parameter: 5) }

  let(:items_page) { ItemsPage.new }
  let(:cart_page) { ShowCartPage.new }

  describe 'Promotion Codes' do
    before(:each) do
      items_page.load
      items_page.items.first.add_to_cart.click
      cart_page.return_to_shop.click
      items_page.items[1].add_to_cart.click
      cart_page.return_to_shop.click
      items_page.items[2].add_to_cart.click
    end

    describe 'Reducing price' do
      it 'reduces price accordinly and can be removed' do
        expect(items_page.items.count).to be 3
        expect(cart_page.total).to have_text 102.33
        cart_page.add_code.set '20%OFF'
        cart_page.submit_code.click
        expect(cart_page.total).to have_text 81.86

        expect(cart_page.promotion_codes.first.name).to have_text '20%OFF'
        cart_page.promotion_codes.first.remove.click
        expect(cart_page.promotion_codes.count).to be 0
        expect(cart_page.total).to have_text 102.33
      end
    end

    describe 'Solo codes' do
      it 'adds just one' do
        cart_page.add_code.set '20%OFF'
        cart_page.submit_code.click
        expect(cart_page.promotion_codes.length).to be 1
        cart_page.add_code.set '20%OFF'
        cart_page.submit_code.click
        expect(cart_page.promotion_codes.length).to be 1
        cart_page.add_code.set '20POUNDSOFF'
        cart_page.submit_code.click
        expect(cart_page.promotion_codes.length).to be 1
      end
    end

    describe 'non solo codes' do
      it 'adds multiple different ones' do
        cart_page.add_code.set '20POUNDSOFF'
        cart_page.submit_code.click
        expect(cart_page.promotion_codes.length).to be 1
        cart_page.add_code.set '5POUNDSOFF'
        cart_page.submit_code.click
        expect(cart_page.promotion_codes.length).to be 2
      end
    end
  end
end
