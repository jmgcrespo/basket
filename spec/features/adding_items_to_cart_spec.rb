require 'rails_helper'

RSpec.describe 'Shop', type: :feature do
  let!(:item1) { FactoryGirl.create(:item, name: 'Item1', price: 44.11) }
  let!(:item2) { FactoryGirl.create(:item, name: 'Item2', price: 34.11) }
  let!(:item3) { FactoryGirl.create(:item, name: 'Item3', price: 24.11) }
  let(:items_page) { ItemsPage.new }
  let(:cart_page) { ShowCartPage.new }

  describe 'shop' do
    it 'list items and details' do
      items_page.load
      expect(items_page.items.count).to be 3
      items = items_page.items

      expect(items[0].name).to have_text 'Item1'
      expect(items[0].price).to have_text 44.11
      expect(items[0]).to have_add_to_cart

      expect(items[1].name).to have_text 'Item2'
      expect(items[1].price).to have_text 34.11
      expect(items[2].name).to have_text 'Item3'
      expect(items[2].price).to have_text 24.11
    end
  end

  describe 'Adding and removing items to/from cart' do
    before(:each) do
      items_page.load
    end

    it 'adds items and displays the cart' do
      items_page.items.first.add_to_cart.click

      expect(cart_page).to be_displayed
      expect(cart_page.items.count).to be 1

      first_cart_item = cart_page.items.first
      expect(first_cart_item.name).to have_text 'Item1'
      expect(cart_page.total).to have_text 44.11

      cart_page.return_to_shop.click

      expect(items_page).to be_displayed
      items_page.items[1].add_to_cart.click

      expect(cart_page).to be_displayed
      expect(cart_page.items.count).to be 2
      first_cart_item = cart_page.items.first
      expect(first_cart_item.name).to have_text 'Item1'
      second_cart_item = cart_page.items[1]
      expect(second_cart_item.name).to have_text 'Item2'
      expect(cart_page.total).to have_text 78.22
    end

    it 'counts and calculate prices for an item added multiple times' do
      items_page.items.first.add_to_cart.click

      expect(cart_page).to be_displayed
      expect(cart_page.items.count).to be 1
      expect(cart_page.items.first.count).to have_text '1'
      expect(cart_page.items.first.price).to have_text 44.11
      cart_page.return_to_shop.click

      items_page.items.first.add_to_cart.click
      expect(cart_page).to be_displayed
      expect(cart_page.items.count).to be 1
      expect(cart_page.items.first.count).to have_text '2'
      expect(cart_page.items.first.price).to have_text 88.22

      cart_page.items.first.remove_from_cart.click
      expect(cart_page).to be_displayed
      expect(cart_page.items.first.count).to have_text '1'
      expect(cart_page.items.first.price).to have_text 44.11

      cart_page.items.first.remove_from_cart.click
      expect(cart_page).to be_displayed
      expect(cart_page.items.count).to be 0
      expect(cart_page.total).to have_text 0.0
    end
  end
end
