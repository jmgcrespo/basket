require 'rails_helper'

RSpec.describe Item, type: :feature do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:price) }
end
