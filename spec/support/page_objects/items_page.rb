class ItemsPage < SitePrism::Page
  set_url '/items'
  sections :items, 'li.item' do
    element :name, 'span.name'
    element :price, 'span.price'
    element :add_to_cart, 'a.add-to-cart'
  end
end
