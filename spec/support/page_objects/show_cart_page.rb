class ShowCartPage < SitePrism::Page
  set_url '/cart'
  sections :items, 'li.item' do
    element :count, 'span.count'
    element :name, 'span.name'
    element :price, 'span.price'
    element :remove_from_cart, 'a.remove-from-cart'
  end
  sections :promotion_codes, 'li.promotion-code' do
    element :name, 'span.name'
    element :remove, 'a.remove-code'
  end

  element :add_code, 'form.add-promotion-code #name'
  element :submit_code, 'input[type="submit"]'
  element :total, 'span.total'
  element :return_to_shop, 'a.return-to-shop'
end
