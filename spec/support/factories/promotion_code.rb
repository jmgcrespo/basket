FactoryGirl.define do
  factory :promotion_code do
    name '20%OFF'
    code_type 'percentage_off'
    parameter 0.2
    solo true

    factory :amount_off do
      name '20POUNDSOFF'
      code_type 'amount_off'
      parameter 20
      solo false
    end
  end
end
